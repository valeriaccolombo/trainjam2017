﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour 
{
	private static GameManager _instance;
	public static GameManager Instance
	{
		get
		{ 
			return _instance;
		}
	}

	public Bondi player1;
	public Bondi player2;
	public PassengersGenerator passengersGenerator;

	public TextMeshProUGUI timerText;
	public TextMeshProUGUI player1CountText;
	public TextMeshProUGUI player2CountText;
	public GameOverPopup gameOVerPopup;

	private int GAME_DURATION = 60; //seconds
	private int elapsedTime;

	public bool IsPlaying { get; private set; }

	void Awake()
	{
		_instance = this;
		gameOVerPopup.gameObject.SetActive (false);
	}

	void Start()
	{
		elapsedTime = 0;
		timerText.text = (GAME_DURATION - elapsedTime) + "secs";

		IsPlaying = false;
	}

	public void StartGame()
	{
		elapsedTime = 0;
		timerText.text = (GAME_DURATION - elapsedTime) + "secs";

		passengersGenerator.Reset ();
		player1.Reset ();
		player2.Reset ();

		IsPlaying = true;
		StartCoroutine (WaitOneSecond());
	}

	IEnumerator WaitOneSecond()
	{
		yield return new WaitForSeconds (1);

		elapsedTime++;
		timerText.text = (GAME_DURATION - elapsedTime) + " secs";

		if (elapsedTime >= GAME_DURATION) 
		{
			IsPlaying = false;
			gameOVerPopup.Show (player1.passngersCollected, player2.passngersCollected);
		}
		else
		{
			StartCoroutine (WaitOneSecond());
		}
	}

	void Update()
	{
		player1CountText.text = player1.passngersCollected.ToString();
		player2CountText.text = player2.passngersCollected.ToString();
	}
}
