﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Passenger : MonoBehaviour
{
	public GameObject angryDialogue;

	public AudioSource audioSrc;
	public AudioClip queja;

	void Awake()
	{
		angryDialogue.SetActive (false);
		StartCoroutine (GetMadIn(Random.Range(15, 25)));
	}

	IEnumerator GetMadIn(int secs)
	{
		yield return new WaitForSeconds (secs);

		audioSrc.clip = queja;
		audioSrc.loop = false;
		audioSrc.Play ();

		angryDialogue.SetActive (true);
		StartCoroutine (HideDialogueInAWhile());
		StartCoroutine (GetMadIn(Random.Range(5, 10)));
	}

	IEnumerator HideDialogueInAWhile()
	{
		yield return new WaitForSeconds (1);
		angryDialogue.SetActive (false);
	}
}

