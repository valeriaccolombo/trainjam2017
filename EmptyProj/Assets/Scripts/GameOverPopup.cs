﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOverPopup : MonoBehaviour 
{
	public TextMeshProUGUI player1ResText;
	public TextMeshProUGUI player2ResText;

	public GameObject MainMenu;

	public void Show(int p1Total, int p2Total)
	{
		player1ResText.text = p1Total.ToString();
		player2ResText.text = p2Total.ToString();
		gameObject.SetActive (true);
	}

	public void OnClickClose()
	{
		gameObject.SetActive (false);
		MainMenu.SetActive (true);
	}
}
