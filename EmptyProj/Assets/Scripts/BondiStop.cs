﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BondiStop : MonoBehaviour 
{
	public List<Passenger> Passengers { get; private set; }

	private float timeLastPassengereUp = 0;
	private float SEPARACION_PASAJEROS = 0.05f;

	void Start()
	{
		Passengers = new List<Passenger> ();
	}
		
	public bool RemovePassenger()
	{
		if ((Time.fixedTime - timeLastPassengereUp) > 1 && Passengers.Count > 0) 
		{
			Passenger p = Passengers [0];
			Passengers.RemoveAt (0);
			Destroy (p.gameObject);

			timeLastPassengereUp = Time.fixedTime;

			foreach (Passenger p2 in Passengers) 
			{
				Vector3 p2Pos = p2.transform.localPosition;
				p2Pos.x -= SEPARACION_PASAJEROS;
				p2.transform.localPosition = p2Pos;
			}

			return true;
		}
		return false;
	}

	public void AddPassenger(GameObject go)
	{
		go.transform.SetParent (transform);
		go.transform.localPosition = new Vector3 (Passengers.Count * SEPARACION_PASAJEROS, 0.1f, 0);

		Passengers.Add (go.GetComponent<Passenger>());
	}

	public void Reset()
	{
		foreach (Passenger p in Passengers) 
		{
			Destroy (p.gameObject);
		}
		Passengers.Clear ();
	}
}
