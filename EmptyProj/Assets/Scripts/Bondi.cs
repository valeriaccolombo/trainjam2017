﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Bondi : MonoBehaviour 
{
	public AudioClip puteada;
	public AudioClip bocina;
	public AudioSource audioSrc;

	public KeyCode ForwardKey = KeyCode.W;
	public KeyCode LeftKey = KeyCode.A;
	public KeyCode BackwardKey = KeyCode.S;
	public KeyCode RightKey = KeyCode.D;
	public KeyCode HornKey = KeyCode.Z;

	public float Aceleration = 0.05f;
	public float RotationSpeed = 1;

	public GameObject angryDialogue;

	public Rigidbody2D rgbody;

	private float lerpFactor = 0.2f;
	public int passngersCollected { get; private set; }

	private List<BondiStop> paradasColliding = new List<BondiStop>();

	void Start()
	{
		Reset ();
	}

	public void Reset()
	{
		passngersCollected = 0;
		angryDialogue.SetActive (false);
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		BondiStop parada = col.gameObject.GetComponent<BondiStop> ();
		if (parada != null && !paradasColliding.Contains (parada)) 
		{
			paradasColliding.Add (parada);
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		BondiStop parada = col.gameObject.GetComponent<BondiStop> ();
		if (parada != null && paradasColliding.Contains (parada)) 
		{
			paradasColliding.Remove (parada);
		}
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.collider.tag == "Train") 
		{
			if (!angryDialogue.activeSelf) 
			{
				audioSrc.clip = puteada;
				audioSrc.loop = false;
				audioSrc.Play ();

				angryDialogue.SetActive (true);
				StartCoroutine (HideAngryDialogueInAWhile());
			}
		}
	}

	IEnumerator HideAngryDialogueInAWhile()
	{
		yield return new WaitForSeconds (3);

		angryDialogue.SetActive (false);
	}

	private void Update ()
	{
		if (!GameManager.Instance.IsPlaying)
			return;
		
		//Movement
		Vector2 force = Vector2.zero;
		if (Input.GetKey (ForwardKey)) 
		{
			force = new Vector2 (transform.right.x * Aceleration, transform.right.y * Aceleration);
		}
		else if (Input.GetKey (BackwardKey)) 
		{
			force = new Vector2(transform.right.x * -Aceleration, transform.right.y * -Aceleration);
		}
		if(Input.GetKeyUp(ForwardKey))
		{
			rgbody.velocity = Vector2.zero;
		}

		Vector2 rotDeacc = new Vector2 (Mathf.Lerp(rgbody.velocity.x, 0, lerpFactor), Mathf.Lerp(rgbody.velocity.y, 0, lerpFactor));

		//Rotation
		if (Input.GetKey (LeftKey))
		{
			transform.Rotate(0, 0, RotationSpeed);
			force = Vector2.zero;
			rgbody.velocity = rotDeacc;
		}
		else if (Input.GetKey (RightKey)) 
		{
			transform.Rotate(0, 0, -RotationSpeed);
			force = Vector2.zero;
			rgbody.velocity = rotDeacc;
		}
		else
		{
			rgbody.inertia = 0;
		}

		rgbody.AddForce (force);

		CheckBusStops ();

		if (Input.GetKeyDown (HornKey)) 
		{
			audioSrc.clip = bocina;
			audioSrc.loop = false;
			audioSrc.Play ();
		}
	}

	private void CheckBusStops()
	{
		if (rgbody.velocity.magnitude < 30)
		{
			foreach (BondiStop stop in paradasColliding) 
			{
				if (stop.Passengers.Count > 0) 
				{
					if(stop.RemovePassenger ())
						passngersCollected++;
				}
			}
		}
	}
}
