﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour 
{
	public GameObject CreditsScreen;

	void Start () 
	{
		gameObject.SetActive (true);
		CreditsScreen.SetActive (false);
	}

	public void OnPlayClick()
	{
		GameManager.Instance.StartGame ();
		gameObject.SetActive (false);
	}

	public void OnCreditsClick()
	{
		CreditsScreen.SetActive (true);
	}
}
