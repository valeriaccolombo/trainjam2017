﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassengersGenerator : MonoBehaviour 
{
	private BondiStop[] paradas;

	void Start()
	{
		paradas = GameObject.FindObjectsOfType<BondiStop> ();

		StartCoroutine (SpawnNewPassenger());
	}

	IEnumerator SpawnNewPassenger()
	{
		yield return new WaitForSeconds (Random.Range(2, 5));

		if (GameManager.Instance.IsPlaying) 
		{
			GameObject passenger = Instantiate (Resources.Load<GameObject>(string.Format("Prefabs/Passenger_{0}", Random.Range(1,7))));
			paradas [Random.Range (0, paradas.Length)].AddPassenger (passenger);

			StartCoroutine (SpawnNewPassenger());
		}
	}

	public void Reset()
	{
		foreach(BondiStop p in paradas)
		{
			p.Reset ();
		}
		StartCoroutine (SpawnNewPassenger());
	}
}
