﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barreras_controller : MonoBehaviour 
{
	public Animator train;
    public float tiempodetren = 5, tiempoentretrenes= 25;
	private barreras[] barrerass;

	public AudioSource audioSrc;
	public AudioClip barrerasBajan;
	public AudioClip choochoo;

    void Start()
	{
		barrerass = gameObject.GetComponentsInChildren<barreras> ();
		StartCoroutine (WaitAndSpawnTrain());
    }

	IEnumerator WaitAndSpawnTrain()
	{
		//Debug.Log ("Esperar " + tiempoentretrenes + " antes de bajar la barrera");
		yield return new WaitForSeconds (tiempoentretrenes);

		if (GameManager.Instance.IsPlaying) 
		{
			//Debug.Log ("bajar la barrera");
			audioSrc.clip = barrerasBajan;
			audioSrc.loop = false;
			audioSrc.Play ();

			train.Play ("choochoo");
			foreach (barreras b in barrerass) 
			{
				b.BajarBarreras ();
			}

			StartCoroutine (WaitAndOpenBarriers());
		}
		else
		{
			StartCoroutine (WaitAndSpawnTrain());
		}
	}

	IEnumerator WaitAndOpenBarriers()
	{
		//Debug.Log ("Esperar " + tiempodetren + " antes de subir la barrera");
		yield return new WaitForSeconds (tiempodetren*0.7f);

		audioSrc.clip = choochoo;
		audioSrc.loop = false;
		audioSrc.Play ();

		yield return new WaitForSeconds (tiempodetren*0.3f);

		//Debug.Log ("subir la barrera");
		foreach (barreras b in barrerass) 
		{
			b.SubirBarreras ();
		}

		StartCoroutine (WaitAndSpawnTrain());
	}
}
