﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class barreras : MonoBehaviour {

	private BoxCollider2D col;
	private GameObject barrera1Img;
	private GameObject barrera2Img;

	void Start () 
	{
		barrera1Img = transform.Find ("barrera1").gameObject;
		barrera2Img = transform.Find ("barrera2").gameObject;
		col = GetComponent<BoxCollider2D> ();

		SubirBarreras ();
	}
	
	public void SubirBarreras()
	{
		barrera1Img.SetActive (false);
		barrera2Img.SetActive (false);
		col.enabled = false;
	}

    public void BajarBarreras()
    {
		barrera1Img.SetActive (true);
		barrera2Img.SetActive (true);
		col.enabled = true;
    }
}
