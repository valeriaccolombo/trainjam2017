﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawners : MonoBehaviour 
{
    public float velocidadx, velocidady;
	public string PrefabAutoName = "Autohorizontal";
    private GameObject spawned;

    public void spawn()
    {
		Debug.Log ("Spawn car " + gameObject.name);
		spawned = (GameObject)Instantiate(Resources.Load<GameObject>(string.Format("Prefabs/{0}_{1}",PrefabAutoName,Random.Range(1,5))),transform.position,transform.rotation);
		spawned.GetComponent<Auto>().movespeed = new Vector2(velocidady*0.02f, velocidadx*0.02f);
    }
}
