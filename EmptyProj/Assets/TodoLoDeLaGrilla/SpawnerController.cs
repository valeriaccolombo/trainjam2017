﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour {

    public float spawntimer;
    private float timer=0;
    private Spawners spawner;
    private Spawners[] spawners;
	
	void Update () {
        timer += Time.deltaTime;
        if(timer >= spawntimer)
        {
            spawners = GetComponentsInChildren<Spawners>();
            spawner = spawners[Random.Range(0, spawners.Length-1)];
            spawner.spawn();
            timer = 0;
            //(GameObject) Instantiate()
        }
	}
}
