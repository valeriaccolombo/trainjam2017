﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Auto : MonoBehaviour {
    public Vector2 movespeed;

	public void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("colisiono");
        //Debug.Log(collision.gameObject.tag);
        if(collision.gameObject.tag == "Limit")
        {
            Destroy(gameObject);
        }
    }

	void Update () {
        GetComponent<Rigidbody2D>().AddForce(movespeed,ForceMode2D.Impulse);
        //transform.Translate(movespeed.x, movespeed.y, 0);
	}
}
